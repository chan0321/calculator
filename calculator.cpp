#include <iostream>
#include <vector>
using namespace std ;

class Equation
{
public:
    vector <double> nums;   //数字容器
    vector <char> symbols;  //符号容器
    double result = 0.0;    //计算结果
    char m_exit = '1';      //0 退出，1 继续计算，其他值 清空再计算
    bool i = true;          //是否需要输入数字
    void input()
    {
        double m_num;
        char m_symbol;    
        nums.clear();
        symbols.clear();
        while(true)
        {
            if(i)
            {
                cout << "请输入数字"<< endl;
                if(cin >> m_num)     //成功输入数字
                {
                    if(symbols.size() > 0)      //符号容器不为空
                    {
                        char lastsymbol = *(symbols.end() - 1);    //指向最后一个符号
                        
                        //当除数为0时（/ 0）循环输入直到除数不为0
                        while(lastsymbol == '/' && m_num == 0 )                          
                        {
                            if(cin.fail())
                            {
                                cout<<"输入错误，请输入数字"<<endl;
                            }
                            else
                            {
                                cout << "除数不能为0，请重新输入" << endl; 
                            }                                                     
                            cin.clear();
                            cin.ignore(100,'\n');
                            cin>>m_num; 
                        } 
                        cin.clear();
                        cin.ignore(100,'\n'); 
                    }
                    nums.push_back(m_num);      //将数字放入容器
                }
                else    //输入的不是数字时
                {
                    cout << "输入错误" << endl;
                    cin.clear();
                    cin.ignore(100,'\n');   //清空输入流缓冲区
                    continue;
                }
            }
            else
            {
                nums.push_back(result);
                i = true;
                result = 0.0;
            }         
            cout << "请输入符号" << endl;
            cin>> m_symbol;          
            while(m_symbol != '+' && m_symbol != '-' &&
            m_symbol != '*' && m_symbol != '/' && m_symbol != '=')  //输入不是指定符号时重新输入
            {
                cout<<"输入错误符号，请重新输入"<<endl;
                cin.clear();
                cin.ignore(100,'\n');
                cin>>m_symbol;
            }
            cin.clear();
            cin.ignore(100,'\n'); 
            if(m_symbol =='=')      //输入等号后结束输入
            {
                break;
            }  
            symbols.push_back(m_symbol);    //将符号放入容器
        }
    }
    void calculating()
    {
        int j=0;
        vector<char>::iterator it1 = symbols.begin();
        while(it1 != symbols.end())
        {
            //乘，将两数相乘的结果代替原来的两个数，并删除符号容器中此处的乘号*
            if(*it1 == '*')
            {
                nums[j+1] = nums[j] * nums[j+1];   
                it1 = symbols.erase(it1);            
                vector<double> ::iterator i=nums.begin();
            	nums.erase(nums.begin()+j);
            }
            //除，将两数相除的结果代替原来的两个数，并删除符号容器中此处的除号/
            else if(*it1 == '/')
            {
                nums[j+1] = nums[j] / nums[j+1];
                it1 = symbols.erase(it1);
                vector<double> ::iterator i=nums.begin();
            	nums.erase(nums.begin()+j);
            }
            else
            {
                it1++;
                j++;
            }            
        }
        int k = 1;        
        result = nums[0];
        for(vector<char>::iterator it2 = symbols.begin();
        it2 != symbols.end();it2++)
        {
            if(*it2=='+')
            {
                result += nums[k];  //加
            }
            if(*it2=='-')
            {
                result -= nums[k];  //减
            }
            k++;
        }
    }
    void mContinue()
    {
        cout << "请输入一个字符，输入0退出计算器,输入1继续计算，输入其他值清空并重新计算" <<endl;
        cin >> m_exit;
        i = false;
        cin.clear();
        cin.ignore(100,'\n');
        if(m_exit != '1')
        {
            i = true;
        } 
    }
};

int main()
{
    Equation e;
    cout << "计算器小程序，输入 “=” 输出计算结果" <<endl;
    while(e.m_exit != '0')
    {
        e.input();
        e.calculating();
        cout << "计算结果："<< e.result << endl;
        e.mContinue();
    }
}